export const Injectable = (): ClassDecorator => {
	return target => {
		target.prototype.diInjectable = true;
	};
};