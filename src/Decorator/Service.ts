/**
 * Decorator alias for {@link Injectable}
 * @constructor
 */
export const Service = (): ClassDecorator => {
	return target => {
		target.prototype.diInjectable = true;
	};
};