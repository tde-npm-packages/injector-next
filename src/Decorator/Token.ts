export interface TokenParams {
	factory: () => any;
}

export const Token = (options: TokenParams | string): ParameterDecorator => {
	return (target: Object, propertyKey: string | symbol, parameterIndex: number) => {
		const tokens = Reflect.getMetadata('design:paramtypes', target) || [];

		tokens[parameterIndex] = typeof options === 'string' ? options : options.factory;
		// redefine param types, so we can check where is our namespace suppose to be injected
		Reflect.defineMetadata('design:paramtypes', tokens, target);
	};
};