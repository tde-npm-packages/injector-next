import 'reflect-metadata';
import { ResolverType } from './Type/ResolverType';

export class Injector {
	protected static services: Map<string, any> = new Map<string, any>();
	protected static customResolvers: ResolverType[] = [];
	protected static serviceCount = 0;

	static registerResolver(resolver: ResolverType) {
		Injector.customResolvers.push(resolver);
	}

	static get(name: string) {
		if (Injector.services.has(name)) {
			return Injector.services.get(name);
		}

		return null;
	}

	static resolve<T>(
		target: new(...args: any[]) => T,
		customResolve?: ResolverType,
		newInstance = false
	): T {
		if (!newInstance) {
			for (const [, resolved] of Injector.services) {
				if (resolved instanceof target) {
					return resolved;
				}
			}
		}

		const tokens = Reflect.getMetadata('design:paramtypes', target) || [];

		// Attempt to resolve dependencies
		const injections = [];
		for (const idx in tokens) {
			if (!Object.prototype.hasOwnProperty.call(tokens, idx)) {
				continue;
			}

			const token = tokens[idx];
			let tokenResolved;
			if (typeof customResolve === 'function') {
				tokenResolved = customResolve(token, idx, tokens);
			}

			for (const resolve of Injector.customResolvers) {
				tokenResolved = resolve(token, idx, tokens);
				if (tokenResolved) {
					break;
				}
			}

			if (typeof token === 'string') {
				tokenResolved = Injector.get(token);
			}

			if (token.diFactory) {
				tokenResolved = token();
			}

			if (!tokenResolved) {
				tokenResolved = Injector.resolve<any>(token);
			}

			if (!tokenResolved) {
				throw new Error(`Unable to resolve dependency ${ token.name } (I: ${ idx }) in: ${ target.name }`);
			}

			injections.push(tokenResolved);
		}

		const instance = new target(...injections);
		if (newInstance) {
			return instance;
		}

		const newName = 'internal-' + Injector.serviceCount++;
		Injector.services.set(newName, instance);

		return instance;
	}

	static register(name: string, service: any) {
		if (Injector.services.has(name)) {
			throw new Error(`Service with name: ${ name } was already registered`);
		}

		Injector.services.set(name, service);
	}

	static override(name: string, service: any) {
		Injector.services.set(name, service);
	}
}