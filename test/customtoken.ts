import { Token, TokenParams } from '../src/Decorator/Token';
import { Injector }           from '../src/Injector';
import { Injectable } from '../src/Decorator/Injectable';

interface CustomOptions {
	min: number;
	max: number;
	amount: number;
}

const CustomToken = (options: CustomOptions): ParameterDecorator => {
	return (target: Object, propertyKey: string | symbol, parameterIndex: number) => {
		// collect existing param types
		const tokens = Reflect.getMetadata('design:paramtypes', target) || [];

		// make new factory
		const factory = () => {
			const out = [];
			for (let i = 0; i < options.amount; i++) {
				out.push(Math.random() * (options.max - options.min) + options.min)
			}

			return out;
		};
		// mark it as factory
		factory.diFactory = true;

		tokens[parameterIndex] = factory;
		// redefine param types so we can check where is our namespace suppose to be injected
		Reflect.defineMetadata('design:paramtypes', tokens, target);
	};
};

@Injectable()
class ServiceB {
	constructor(
		@CustomToken({ min: 1, max: 10, amount: 3 })
		protected arr: number[],

		@CustomToken({ min: 10, max: 20, amount: 5 })
		protected arr2: number[],

		@CustomToken({ min: 100, max: 200, amount: 7 })
		protected arr3: number[],
	) {}
}

const sb = Injector.resolve(ServiceB);
console.log(sb);