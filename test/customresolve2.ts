import { Injector }   from '../src/Injector';
import { Injectable } from '../src/Decorator/Injectable';
import 'reflect-metadata';

export class OmniORM {}
Injector.register('orm', new OmniORM());

class PostgresRepository<T> {
	constructor(public type: any) {
	}
}

class SomeModel {}

export class OmniRepositoryToken {
	constructor(
		public repoClass: any,
		public modelClass?: any,
		public ormName: string = 'orm',
	) {
	}
}

export class OmniOrmToken {
	constructor(
		public ormName: string = 'orm',
	) {
	}
}


export function OmniRepository(repoClass: any, modelClass?: any, ormName?: string) {
	return (target: any, propertyKey: string, parameterIndex: number) =>{
		const tokens = Reflect.getMetadata('design:paramtypes', target) || [];

		tokens[parameterIndex] = new OmniRepositoryToken(repoClass, modelClass, ormName);

		// redefine param types so we can check where is our namespace suppose to be injected
		Reflect.defineMetadata('design:paramtypes', tokens, target);
	}
}

export function Omni(ormName?: string) {
	return (target: any, propertyKey: string, parameterIndex: number) =>{
		const tokens = Reflect.getMetadata('design:paramtypes', target) || [];

		tokens[parameterIndex] = new OmniOrmToken(ormName);

		// redefine param types so we can check where is our namespace suppose to be injected
		Reflect.defineMetadata('design:paramtypes', tokens, target);
	}
}

const customResolve = (token: any, idx: string) => {
	if (token instanceof OmniRepositoryToken) {
		return new token.repoClass(token.modelClass);
	}

	if (token instanceof OmniOrmToken) {
		return Injector.get(token.ormName);
	}


	return null;
};

Injector.registerResolver(customResolve);

@Injectable()
class ServiceB {
	constructor(
		@Omni() public orm: any,
		@OmniRepository(PostgresRepository, SomeModel) protected rep: PostgresRepository<SomeModel>,
	) {}
}

const sb = Injector.resolve(ServiceB);
console.log(sb);