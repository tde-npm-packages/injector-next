import { Token }      from '../src/Decorator/Token';
import { Injector }   from '../src/Injector';
import { Injectable } from '../src/Decorator/Injectable';

// create a factory class
const randFactory = () => { return Math.random(); }
randFactory.diFactory = true;

@Injectable()
class ServiceX {
	stable = Math.random();
}

@Injectable()
class Entity {
	constructor(
		protected serv: ServiceX,
		@Token({ factory: randFactory })
		protected rand: number
	) {}
}

// Those will be just instanciated but not kept in registry
const e1 = Injector.resolve(Entity, null, true);
const e2 = Injector.resolve(Entity, null, true);
const e3 = Injector.resolve(Entity, null, true);

console.log(e1);
console.log(e2);
console.log(e3);