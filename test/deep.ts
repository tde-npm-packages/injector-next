import { Injector }   from '../src/Injector';
import { Injectable } from '../src/Decorator/Injectable';

@Injectable()
class ClassX {
	x = 999;
}

@Injectable()
class ClassY {
	y = 111;
}

@Injectable()
class ServiceA {
	a = 1;
	constructor(
		public x: ClassX,
		public y: ClassY,
	) {}
}

@Injectable()
class ServiceB {
	b = 1;
	constructor(
		public a: ServiceA,
	) {}
}

@Injectable()
class TheService {
	constructor(
		public b: ServiceB,
	) {}
}

const sb = Injector.resolve(TheService);
console.log(sb);
console.log(sb.b.a.x.x);
console.log(sb.b.a.y.y);