import { Token }      from '../src/Decorator/Token';
import { Injector }   from '../src/Injector';
import { Injectable } from '../src/Decorator/Injectable';

const mapFactory = () => { return new Map(); };
mapFactory.diFactory = true;

Injector.register('something', new Date());
Injector.register('somethingTwo', new Date(Date.UTC(2005, 5, 5)));

@Injectable()
class ServiceB {
	constructor(
		@Token({ factory: mapFactory }) protected map: Map<any, any>,
		@Token('something') protected something: Date,
		@Token('somethingTwo') protected somethingTwo: Date
	) {}
}

const sb = Injector.resolve(ServiceB);
console.log(sb);