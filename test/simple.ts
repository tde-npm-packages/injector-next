import { Injector, Injectable }   from '../dist';

@Injectable()
class ServiceA {
	a = 1;
}

@Injectable()
class ServiceB {
	constructor(
		protected sa: ServiceA
	) {}
}

const sb = Injector.resolve(ServiceB);
console.log(ServiceA, ServiceA.prototype);
console.log(sb);