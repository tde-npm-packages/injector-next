import { Injector, Injectable, Token }   from '../dist';

const singletonInstance = {
	unnamed: 'object',
	that: 'has no constructor'
}
const configFactory = () => { return singletonInstance; }
configFactory.diFactory = true;

@Injectable()
class ServiceB {
	constructor(
		// Mark parameter directly
		@Token({ factory: configFactory }) protected config: any
	) {}
}

const sb = Injector.resolve(ServiceB);
console.log(sb);