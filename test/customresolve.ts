import { Injector }   from '../src/Injector';
import { Injectable } from '../src/Decorator/Injectable';
import { inspect }    from 'util';

class ServiceExternal {
	a = 1;
}

@Injectable()
class ServiceB {
	constructor(
		protected sa: any,
		protected cons: typeof console,
		protected dt: Date,
	) {}
}

const customResolve = (token: any, idx: string) => {
	if (+idx === 0) {
		return new ServiceExternal();
	}
	if (+idx === 1) {
		return console;
	}
	if (token === Date) {
		return new Date();
	}

	return null;
};

const sb = Injector.resolve(ServiceB, customResolve);
console.log(sb);